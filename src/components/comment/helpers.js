import { nextView } from '../../store/view';
import { localStorage, ADD_NAME, COMMENT_BOX, STORAGE_MR_ID } from '../../shared';
import { clearNote } from '../note/helpers';
import { addForm } from '../wrapper/helpers';

const clearMrId = state => {
  localStorage.removeItem(STORAGE_MR_ID);
  state.mergeRequestId = '';
};

const changeSelectedMr = state => {
  clearMrId(state);
  clearNote();
  addForm(nextView(state, COMMENT_BOX));
};

const addNameToComment = state => {
  addForm(nextView(state, ADD_NAME));
};

const goToFeedbackForm = state => {
  addForm(nextView(state, COMMENT_BOX));
};

export { addNameToComment, changeSelectedMr, clearMrId, goToFeedbackForm };
