import { selectCommentBox } from './../utils';
import { sessionStorage, STORAGE_COMMENT } from '../../shared';

export const getSavedComment = () => sessionStorage.getItem(STORAGE_COMMENT) || '';

export const saveComment = (state = {}) => {
  const commentBox = selectCommentBox();

  // This may be added to any view via top-level beforeunload listener
  // so let's skip if it does not apply
  if (commentBox && commentBox.value) {
    const currentComment = commentBox.value.trim();
    state.comment = currentComment;
    sessionStorage.setItem(STORAGE_COMMENT, currentComment);
  }
};

export const clearSavedComment = () => {
  sessionStorage.removeItem(STORAGE_COMMENT);
};
