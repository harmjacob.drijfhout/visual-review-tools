import {
  ADD_NAME_BUTTON,
  ADD_NAME_CANCEL_BUTTON,
  ADD_PAT_BUTTON,
  ADD_PAT_CANCEL_BUTTON,
} from '../../shared';
import { feedbackHeader } from '../form_elements';
import { patField } from '../pat';
import { nameFields } from '../name';
import { buttonClearStyles } from '../utils';

const submitMessage = 'Submit review';
const cancelMessage = 'Change review';

export const userInfoForm = state => {
  const { requireAuth } = state;
  const requireAuthBool = requireAuth === 'true';

  const submitButtonId = requireAuthBool ? ADD_PAT_BUTTON : ADD_NAME_BUTTON;
  const cancelButtonId = requireAuthBool ? ADD_PAT_CANCEL_BUTTON : ADD_NAME_CANCEL_BUTTON;

  return `
    <div>
      ${feedbackHeader(state.mergeRequestId)}
      ${requireAuthBool ? patField(state) : nameFields(state)}
      <div class="gl-flex gl-justify-flex-end gl-mt-4">
        <button class="gitlab-button gitlab-button-secondary" style="${buttonClearStyles}" type="button" id="${cancelButtonId}">${cancelMessage}</button>
        <button class="gitlab-button gitlab-button-success" style="${buttonClearStyles}" type="button" id="${submitButtonId}">${submitMessage}</button>
      </div>
    </div>
  `;
};
