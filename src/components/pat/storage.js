import { selectAddPatBox } from '../utils';

export const getPersonalAccessToken = (state = {}) => state.personalAccessToken || '';

export const savePersonalAccessToken = state => {
  const patBox = selectAddPatBox();

  if (patBox && patBox.value) {
    const personalAccessToken = patBox.value.trim();
    state.personalAccessToken = personalAccessToken;
  }
};
