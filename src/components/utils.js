import {
  ADD_NAME,
  ADD_EMAIL,
  ADD_PAT,
  COMMENT_BOX,
  COMMENT_BUTTON,
  EXPANSION_BUTTON_CONTAINER,
  EXPANSION_CONTAINER,
  FORM,
  FORM_CONTAINER,
  MR_ID,
  NOTE,
  NOTE_CONTAINER,
  REMEMBER_ITEM,
  REVIEW_CONTAINER,
} from '../shared';

// this style must be applied inline in a handful of components
export const buttonClearStyles = `
  -webkit-appearance: none;
`;

// selector functions to abstract out a little
export const selectAddNameBox = () => document.getElementById(ADD_NAME);
export const selectAddEmailBox = () => document.getElementById(ADD_EMAIL);
export const selectAddPatBox = () => document.getElementById(ADD_PAT);
export const selectById = id => document.getElementById(id);
export const selectCommentBox = () => document.getElementById(COMMENT_BOX);
export const selectCommentButton = () => document.getElementById(COMMENT_BUTTON);
export const selectContainer = () => document.getElementById(REVIEW_CONTAINER);
export const selectExpansionButtonContainer = () =>
  document.getElementById(EXPANSION_BUTTON_CONTAINER);
export const selectExpansionContainer = () => document.getElementById(EXPANSION_CONTAINER);
export const selectForm = () => document.getElementById(FORM);
export const selectFormContainer = () => document.getElementById(FORM_CONTAINER);
export const selectMrBox = () => document.getElementById(MR_ID);
export const selectNote = () => document.getElementById(NOTE);
export const selectNoteContainer = () => document.getElementById(NOTE_CONTAINER);
export const selectRemember = () => document.getElementById(REMEMBER_ITEM);
