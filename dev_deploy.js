/* eslint-disable no-console */
/* eslint import/no-commonjs: [2, { allowRequire: true }]  */

const { spawn } = require('child_process');

const child = spawn('now', { cwd: 'dev/review-script-tester' });

console.log('\n 🤞🏽 🤞🏽 🤞🏽 🤞🏽 🤞🏽 🤞🏽 🤞🏽 🤞🏽 🤞🏽 🤞🏽 🤞🏽 🤞🏽 🤞🏽 \n');

child.stdout.on('data', data => {
  console.log(`${data}`);
});

child.stderr.on('data', data => {
  console.error(`${data}`);
});
