import { nameFields } from '~/components/name';
import { setHTMLFixture } from '../../helpers';
import { selectAddNameBox, selectAddEmailBox } from '~/components/utils';

describe('the name input component', () => {
  beforeEach(() => {
    setHTMLFixture(nameFields({}));
  });

  it('contains the name field', () => {
    expect(selectAddNameBox()).toBeInTheDocument();
  });

  it('contains the email field', () => {
    expect(selectAddEmailBox()).toBeInTheDocument();
  });

  it('matches the view snapshot', () => {
    expect(document.body).toMatchSnapshot();
  });
});
