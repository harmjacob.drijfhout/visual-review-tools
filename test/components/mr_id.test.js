import { MR_ID_BUTTON, RED } from '~/shared';
import { mrForm } from '~/components/mr_id';
import { addMr, storeMr } from '~/components/mr_id/helpers';
import { selectMrBox, selectNote, selectCommentBox } from '~/components/utils';
import { createWrapperWithContent, setHTMLFixture } from '../helpers';

const selectMrIdButton = () => document.getElementById(MR_ID_BUTTON);
const addId = id => {
  selectMrBox().value = id;
};

const state = {};
const mergeRequestId = '658719';

describe('the mr id number component', () => {
  beforeEach(() => {
    setHTMLFixture(mrForm);
  });

  describe('the view', () => {
    it('contains the mr field', () => {
      expect(selectMrBox()).toBeInTheDocument();
    });

    it('encodes the field as a text type', () => {
      expect(selectMrBox()).toHaveAttribute('type', 'text');
    });

    it('contains the mr id submit button', () => {
      expect(selectMrIdButton()).toBeInTheDocument();
    });

    it('matches the view snapshot', () => {
      expect(document.body).toMatchSnapshot();
    });
  });

  describe('store mr id', () => {
    it('adds the id to the state', () => {
      storeMr(mergeRequestId, state);
      expect(state).toMatchObject({ mergeRequestId });
    });
  });

  describe('addMr', () => {
    beforeEach(() => {
      createWrapperWithContent(mrForm);
    });

    it('returns an error when there is no mr value', () => {
      addMr(state);
      expect(selectNote().innerText).toBe('Please enter your merge request ID number.');
      expect(selectMrBox()).toHaveStyle(`borderColor: ${RED}`);
    });

    it('does not set state when there is no token value', () => {
      addMr(state);
      expect(state).toMatchObject({});
    });

    it('sets state when there is a token value', () => {
      addId(mergeRequestId);
      addMr(state);
      expect(state).toMatchObject({ mergeRequestId });
    });

    it('moves to next view when there is a token value', () => {
      addId(mergeRequestId);
      addMr({ ...state, token: '3jvudli7' });
      expect(selectCommentBox()).toBeInTheDocument();
    });
  });
});
