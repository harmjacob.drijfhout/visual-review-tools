import { note, wrapper } from '~/components';
import { REVIEW_CONTAINER } from '~/shared';

const resetHTMLFixture = () => {
  document.body.textContent = '';
};

const setHTMLFixture = (htmlContent, resetHook = afterEach) => {
  document.body.outerHTML = htmlContent;
  resetHook(resetHTMLFixture);
};

// TODO: Move a version of this to the index file when toolbar development is just in this project
const createWrapperWithContent = (content, resetHook = afterEach) => {
  const mainContent = wrapper(content);
  const container = document.createElement('div');
  container.setAttribute('id', REVIEW_CONTAINER);
  container.insertAdjacentHTML('beforeend', note);
  container.insertAdjacentHTML('beforeend', mainContent);
  document.body.insertBefore(container, document.body.firstChild);
  resetHook(resetHTMLFixture);
};

export { createWrapperWithContent, setHTMLFixture };
